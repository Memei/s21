// console.log("Hello");

let users = ["Eminem", "Andrew E.", "Flow-G", "Ron Henley"];
console.log("Original Array:");
console.log(users);

function addNewUser(user) {
  users.push(user);
}

addNewUser("Loonie");
console.log(users);


function getUser(index) {
  return users[index];
}

let itemFound = getUser(3);
console.log(itemFound);


function deleteLastUser() {
  let lastUser = users[users.length - 1];
  users.pop();
  return lastUser;
}

const lastUser = deleteLastUser();
console.log(lastUser);
console.log(users)


function updateRappers(index, singer) {
  users[index] = singer;
}

updateRappers(3, "Skusta Klee");

console.log(users);



function deleteUsers() {
  users = [];
}

deleteUsers();
console.log(users);



function checkEmptyArray() {
  if (users.length > 0) {
    return false;
  } else {
    return true;
  }
}

let isUsersEmpty = checkEmptyArray();
console.log(isUsersEmpty);
